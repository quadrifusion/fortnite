import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CodeandplayService {
	@GET("epic-ws/epic/ping")
	Call<String> ping();
	
    @GET("epic-ws/epic/player/getIdEquipe/{nomEquipe}/{motDePasse}")
    Call<String> getIdEquipe(@Path("nomEquipe") String nomEquipe, @Path("motDePasse") String motDePasse);
    
    @GET("epic-ws/epic/versus/next/{idEquipe}")
    Call<String> nouvellePartieContreJoueurs(@Path("idEquipe") String idEquipe);
    
    @GET("epic-ws/epic/practice/new/{numberBot}/{idEquipe}")
    Call<String> nouvellePartieContreBots(@Path("numberBot") String nombreDeBots, @Path("idEquipe") String idEquipe);

    @GET("epic-ws/epic/game/status/{idPartie}/{idEquipe}")
    Call<String> getStatusPartie(@Path("idPartie") String idPartie, @Path("idEquipe") String idEquipe);
    
    @GET("epic-ws/epic/game/board/{idPartie}")
    Call<ResponseBody> getPlateauPartie(@Path("idPartie") String idPartie, @Query("format") String format);
    
    @GET("epic-ws/epic/game/board/{idPartie}/{idEquipe}")
    Call<ResponseBody> getPlateauEquipePartie(@Path("idPartie") String idPartie, @Path("idEquipe") String idEquipe, @Query("format") String format);
    
    @GET("epic-ws/epic/game/getlastmove/{idPartie}/{idEquipe}")
    Call<String> getDernierCoupJoue(@Path("idPartie") String idPartie, @Path("idEquipe") String idEquipe);
    
    @GET("epic-ws/epic/game/play/{idPartie}/{idEquipe}/{move}")
    Call<String> jouerUnTour(@Path("idPartie") String idPartie, @Path("idEquipe") String idEquipe, @Path("move") String action);
    
    @GET("epic-ws/epic/game/opponent/{idPartie}/{idEquipe}")
    Call<String> getNomAdversaire(@Path("idPartie") String idPartie, @Path("idEquipe") String idEquipe);
}
