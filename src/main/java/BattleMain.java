import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BattleMain {

    private String baseUrl;
    private String teamName;
    private String teamPwd;
    private String actionOrc = "REST";
    private String actionPaladin = "REST";

    public int ennemiPrecedent = -1;

    public BattleMain(String[] args) throws Exception {
        this.setConfig(args);
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamPwd() {
        return teamPwd;
    }

    public void setTeamPwd(String teamPwd) {
        this.teamPwd = teamPwd;
    }
    
    public String getActionPaladin() {
        return actionPaladin;
    }

    public void setActionPaladin(String action) {
        this.actionPaladin = action;
    }
    
    public String getActionOrc() {
        return actionOrc;
    }

    public void setActionOrc(String action) {
        this.actionOrc = action;
    }

    public void setConfig(String[] args) throws Exception {
        Options options = new Options();
        options.addOption("p", false, "Pong");
        options.addOption("config", false, "Configuration");
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("p")) {
                System.out.println("pong");
            }
            if (cmd.hasOption("config")) {
                File file = new File(BattleMain.class.getResource("configuration.properties").getFile());

                BufferedReader br = null;
                FileReader fr = null;

                try {
                    fr = new FileReader(file);
                    br = new BufferedReader(fr);

                    String sCurrentLine;

                    while ((sCurrentLine = br.readLine()) != null) {
                        String[] splitted = sCurrentLine.split("=");
                        switch (splitted[0]) {
                            case "rest.base.url":
                                this.setBaseUrl(splitted[1]);
                                break;

                            case "team.name":
                                this.setTeamName(splitted[1]);
                                break;

                            case "team.password":
                                this.setTeamPwd(splitted[1]);
                                break;
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (br != null)
                            br.close();
                        if (fr != null)
                            fr.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            if (cmd.getArgList().size() > 0) {
                throw new Exception("Veuillez entrer le param�tre \"-config\"");
            }
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
    }

    @SuppressWarnings("resource")
    public String getInputPerso() {
        Scanner sc;
        String choix = "";
        do {
            System.out.println("Entrez un perso (ORC, PRIEST, GUARD, CHAMAN, ARCHER, PALADIN) : ");
            sc = new Scanner(System.in);
            choix = sc.next();
            sc.nextLine();
        } while (!choix.equals("ORC") &&
            !choix.equals("PRIEST") &&
            !choix.equals("GUARD") &&
            !choix.equals("CHAMAN") &&
            !choix.equals("ARCHER") &&
            !choix.equals("PALADIN"));
        //sc.close();
        return choix;
    }

    @SuppressWarnings("resource")
    public String getInputModeDeJeu() {
        Scanner sc;
        String choix = "";
        do {
            System.out.println("Entrez un mode de jeu (BOTS, VERSUS) : ");
            sc = new Scanner(System.in);
            choix = sc.next();
            sc.nextLine();
        } while (!choix.equals("BOTS") &&
            !choix.equals("VERSUS"));
        //sc.close();
        return choix;
    }

    @SuppressWarnings("resource")
    public String getInputNiveauBot() {
        Scanner sc;
        String choix = "";
        do {
            System.out.println("Entrez un niveau de difficulté (1 a 6 et 11 a 21) : ");
            sc = new Scanner(System.in);
            choix = sc.next();
            sc.nextLine();
        } while (!choix.equals("1") &&
            !choix.equals("2") &&
            !choix.equals("3") &&
            !choix.equals("4") &&
            !choix.equals("5") &&
            !choix.equals("5") &&
            !choix.equals("6") &&
            !choix.equals("11") &&
            !choix.equals("12") &&
            !choix.equals("13") &&
            !choix.equals("14") &&
            !choix.equals("15") &&
            !choix.equals("16") &&
            !choix.equals("17")&&
            !choix.equals("18") &&
            !choix.equals("19") &&
            !choix.equals("20") &&
            !choix.equals("21"));
        //sc.close();
        return choix;
    }

    public String getAction(String statut, ArrayList < ArrayList < Hero >> plateauAnalyse, int nbTour) {
        String action = "rien";
        switch (statut) {
            case "CANPLAY":
                switch (nbTour) {
                    case 1:
                    case 2:
                    case 3:
                        action = this.getInputPerso();
                        break;
                    default:
                        action = this.ia(plateauAnalyse, nbTour);
                        break;
                }
                break;
            case "CANTPLAY":
            case "VICTORY":
            case "DRAW":
            case "DEFEAT":
            default:
                action = "rien";
        }
        return action;
    }

    public String ia(ArrayList < ArrayList < Hero >> plateauAnalyse, int nbTour) {
        String action = "";
        ArrayList < Hero > herosJoueur = plateauAnalyse.get(0);

        for (int p = 0; p < herosJoueur.size(); p++) {

			if (herosJoueur.get(p).getFighterClass().equals("ORC")) {
				action += this.iaOrc(p, plateauAnalyse, nbTour);
            }
            if (herosJoueur.get(p).getFighterClass().equals("PRIEST")) {
				action += BattleMain.iaPriest(p, plateauAnalyse, nbTour);
            }
            if (herosJoueur.get(p).getFighterClass().equals("GUARD")) {
				action += this.iaGuard(p, plateauAnalyse, nbTour);
            }
            if (herosJoueur.get(p).getFighterClass().equals("ARCHER")) {
				action += this.iaArcher(p, plateauAnalyse, nbTour);
            }
            if (herosJoueur.get(p).getFighterClass().equals("PALADIN")) {
				action += this.iaPaladin(p, plateauAnalyse, nbTour);
            }

            // ajouter ici les ia des nouveaus bots
            
    		if (p < herosJoueur.size() - 1) {
    			action += "$";
    		}
        }

        if (action.indexOf("rien") >= 0) {
            action = "rien";
        }
        System.out.println("ACTION :" + action);
        return action;
    }
    
    public static int iaGetCiblePrioritaire(int index, ArrayList < ArrayList < Hero >> plateauAnalyse, int nbTour, ArrayList<Integer> burningHeroesIds) {
        ArrayList < Hero > herosJoueur1 = plateauAnalyse.get(0);
        ArrayList < Hero > herosJoueur2 = plateauAnalyse.get(1);
        boolean priest = false;
        int idPriest = -1;
        boolean guard = false;
        int idGuard = -1;
        boolean orc = false;
        int idOrc = -1;
        boolean chaman = false;
        int idChaman = -1;
        boolean archer = false;
        int idArcher = -1;
        boolean paladin = false;
        int idPaladin = -1;
        
        for (int he = 0; he < herosJoueur2.size(); he++) {
            if (herosJoueur2.get(he).getFighterClass().equals("PRIEST") && !herosJoueur2.get(he).isDead()) {
                priest = true;
                idPriest = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("ORC") && !herosJoueur2.get(he).isDead()) {
                orc = true;
                idOrc = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("GUARD") && !herosJoueur2.get(he).isDead()) {
                guard = true;
                idGuard = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("CHAMAN") && !herosJoueur2.get(he).isDead()) {
                chaman = true;
                idChaman = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("ARCHER") && !herosJoueur2.get(he).isDead()) {
                archer = true;
                idArcher = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("PALADIN") && !herosJoueur2.get(he).isDead()) {
                paladin = true;
                idPaladin = he;
            }
        }
        
       
        	if (orc) {	
                  return idOrc;
              } else if (chaman) {
                      return idChaman;
              } else if (priest) {
                      return idPriest;
              } else if (paladin) {
                  return idPaladin;
  		    } else if (archer) {
  		          return idArcher;
  		    } else if (guard) {
  		        return idGuard;
  		}
        return -1;
    }
    
    public static int iaGetCiblePrioritairePaladin(int index, ArrayList < ArrayList < Hero >> plateauAnalyse, int nbTour, ArrayList<Integer> burningHeroesIds) {
        ArrayList < Hero > herosJoueur1 = plateauAnalyse.get(0);
        ArrayList < Hero > herosJoueur2 = plateauAnalyse.get(1);
        boolean priest = false;
        int idPriest = -1;
        boolean guard = false;
        int idGuard = -1;
        boolean orc = false;
        int idOrc = -1;
        boolean chaman = false;
        int idChaman = -1;
        boolean archer = false;
        int idArcher = -1;
        boolean paladin = false;
        int idPaladin = -1;
        
        for (int he = 0; he < herosJoueur2.size(); he++) {
            if (herosJoueur2.get(he).getFighterClass().equals("PRIEST") && !herosJoueur2.get(he).isDead()) {
                priest = true;
                idPriest = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("ORC") && !herosJoueur2.get(he).isDead()) {
                orc = true;
                idOrc = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("GUARD") && !herosJoueur2.get(he).isDead()) {
                guard = true;
                idGuard = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("CHAMAN") && !herosJoueur2.get(he).isDead()) {
                chaman = true;
                idChaman = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("ARCHER") && !herosJoueur2.get(he).isDead()) {
                archer = true;
                idArcher = he;
            }
            if (herosJoueur2.get(he).getFighterClass().equals("PALADIN") && !herosJoueur2.get(he).isDead()) {
                paladin = true;
                idPaladin = he;
            }
        }
        	if (archer) {	
                  return idArcher;
              } else if (paladin) {
                      return idPaladin;
              } else if (chaman) {
                      return idChaman;
              } else if (orc) {
                  return idOrc;
  		    } else if (priest) {
  		          return idPriest;
  		    } else if (guard) {
  		        return idGuard;
  		}
        return -1;
    }
    
    public String iaPaladin(int index, ArrayList < ArrayList < Hero >> plateauAnalyse, int nbTour) {
        String action = "";
        ArrayList < Hero > herosJoueur1 = plateauAnalyse.get(0);
        ArrayList < Hero > herosJoueur2 = plateauAnalyse.get(1);
        int idTarget = BattleMain.iaGetCiblePrioritairePaladin(index, plateauAnalyse, nbTour, null);
        
        if(herosJoueur1.get(index).getCurrentMana() > 1 && getActionPaladin().equals("REST")) {
	        action = BattleMain.genereAction(plateauAnalyse, index, "CHARGE", idTarget, 1);
	        setActionPaladin("CHARGE");
        }
        else if(herosJoueur1.get(index).getCurrentMana() > 0 && getActionPaladin().equals("CHARGE")) {
	        action = BattleMain.genereAction(plateauAnalyse, index, "ATTACK", idTarget, 1);
	        setActionPaladin("ATTACK");
        }
        else if(getActionPaladin().equals("ATTACK")){
	        action = BattleMain.genereAction(plateauAnalyse, index, "REST", index, 0);
	        setActionPaladin("REST");
        }
        else {
        	action = BattleMain.genereAction(plateauAnalyse, index, "REST", index, 0);
        }
        
        return action;
    }

    public String iaOrc(int index, ArrayList < ArrayList < Hero >> plateauAnalyse, int nbTour) {
        String action = "";
        ArrayList < Hero > herosJoueur1 = plateauAnalyse.get(0);
        ArrayList < Hero > herosJoueur2 = plateauAnalyse.get(1);
        int idTarget = BattleMain.iaGetCiblePrioritaire(index, plateauAnalyse, nbTour, null);
        
        if(herosJoueur1.get(index).getCurrentMana() > 1 && getActionOrc().equals("REST")) {
	        action = BattleMain.genereAction(plateauAnalyse, index, "YELL", idTarget, 1);
	        setActionOrc("YELL");
        }
        else if(herosJoueur1.get(index).getCurrentMana() > 0 && getActionOrc().equals("YELL")) {
	        action = BattleMain.genereAction(plateauAnalyse, index, "ATTACK", idTarget, 1);
	        setActionOrc("ATTACK");
        }
        else if(getActionOrc().equals("ATTACK")){
	        action = BattleMain.genereAction(plateauAnalyse, index, "REST", index, 0);
	        setActionOrc("REST");
        }
        else {
        	action = BattleMain.genereAction(plateauAnalyse, index, "REST", index, 0);
        }
        
        return action;
    }

    public String iaGuard(int index, ArrayList < ArrayList < Hero >> plateauAnalyse, int nbTour) {
        String action = "";
        ArrayList < Hero > heroJoueur1 = plateauAnalyse.get(0);
        ArrayList < Hero > heroJoueur2 = plateauAnalyse.get(1);
        int idPriest = -1;
        boolean priest = false;
        int idOrc = -1;
        boolean orc = false;
        int idArcher = -1;
        boolean archer = false;
        int idChaman = -1;
        boolean chaman = false;
        
        //On regarde quels heros sont encore en vie
        for (int i = 0; i < heroJoueur1.size(); i++) {
            if (heroJoueur1.get(i).getFighterClass().equals("PRIEST") &&
                !heroJoueur1.get(i).isDead()) {
                priest = true;
                idPriest = i;
            }
            if (heroJoueur1.get(i).getFighterClass().equals("ORC") &&
                !heroJoueur1.get(i).isDead()) {
                orc = true;
                idOrc = i;
            }
            if (heroJoueur1.get(i).getFighterClass().equals("ARCHER") &&
                    !heroJoueur1.get(i).isDead()) {
                    archer = true;
                    idArcher = i;
            }
            if (heroJoueur1.get(i).getFighterClass().equals("CHAMAN") &&
                    !heroJoueur1.get(i).isDead()) {
                    chaman = true;
                    idChaman = i;
            }
        }
        
//        if (nbTour < 6 && idPriest != -1) {
//            action = BattleMain.genereAction(plateauAnalyse, index, "ATTACK", idPriest, 1);
//        } else 
        	if (heroJoueur1.get(index).getCurrentMana() >= 2) {
        		int guardTargetIndex = mostInjuredAlly(plateauAnalyse);
        		System.out.println(guardTargetIndex);
            if (orc && idOrc == guardTargetIndex ) {
                action = BattleMain.genereAction(plateauAnalyse, index, "PROTECT", idOrc, 0);
            } else if(archer && idArcher == guardTargetIndex) {
            	 action = BattleMain.genereAction(plateauAnalyse, index, "PROTECT", idArcher, 0);
            } else if(chaman && idChaman == guardTargetIndex) {
            	action = BattleMain.genereAction(plateauAnalyse, index, "PROTECT", idChaman, 0);
            } else if (priest && idPriest == guardTargetIndex) {
                action = BattleMain.genereAction(plateauAnalyse, index, "PROTECT", idPriest, 0);
            } else {
                int idTarget = BattleMain.iaGetCiblePrioritaire( index,  plateauAnalyse, nbTour, null);
            	action = BattleMain.genereAction(plateauAnalyse, index, "ATTACK", idTarget, 1);
            }
        } else {
            if (!priest && !orc && !archer) {
            	int idTarget = BattleMain.iaGetCiblePrioritaire( index,  plateauAnalyse, nbTour, null);
            	action = BattleMain.genereAction(plateauAnalyse, index, "ATTACK", idTarget, 1);
                
            } else {
                action = BattleMain.genereAction(plateauAnalyse, index, "REST", index, 0);
            }
        }

        return action;
    }

    public static String iaPriest(int index, ArrayList < ArrayList < Hero >> plateauAnalyse, int nbTour) {
        String action = "";
        ArrayList < Hero > heroJoueur1 = plateauAnalyse.get(0);
        Hero priest = heroJoueur1.get(index);

        int priestTargetIndex = mostInjuredAlly(plateauAnalyse);

        System.out.println("Most INJURED = " + heroJoueur1.get(priestTargetIndex).getFighterClass());

        System.out.println("HERO SOIGN� = " + heroJoueur1.get(priestTargetIndex).getFighterClass());

        if (priestTargetIndex != -1) {
            if (priest.getCurrentMana() >= 2) {
                action = BattleMain.genereAction(plateauAnalyse, index, "HEAL", priestTargetIndex, 0);
            } else {
                action = BattleMain.genereAction(plateauAnalyse, index, "REST", index, 0);
            }
        }

        return action;
	}
	
    public static int mostInjuredAlly(ArrayList < ArrayList < Hero >> plateauAnalyse) {
        ArrayList < Hero > heroJoueur1 = plateauAnalyse.get(0);
        Integer mostInjuredHeroIndex = 0;
        int minHealth = 1000;

        for (int i = 0; i < heroJoueur1.size(); i++) {
            if (heroJoueur1.get(i).getCurrentLife() < minHealth && !heroJoueur1.get(i).isDead()) {
                minHealth = heroJoueur1.get(i).getCurrentLife();
                mostInjuredHeroIndex = i;
            }
        }
        return mostInjuredHeroIndex;
    }
    
    public String iaArcher(int index, ArrayList < ArrayList < Hero >> plateauAnalyse, int nbTour) {
        String action = "";
        ArrayList < Hero > heroJoueur1 = plateauAnalyse.get(0);
        ArrayList < Hero > heroJoueur2 = plateauAnalyse.get(1);
        System.out.println(this.ennemiPrecedent);
        Hero archer = heroJoueur1.get(index);
        int archerTargetIndex = -1;
        if (this.ennemiPrecedent == -1) {
        	archerTargetIndex = BattleMain.iaGetCiblePrioritaire(index, plateauAnalyse, nbTour, null);
        	this.ennemiPrecedent = archerTargetIndex;
        	System.out.println(this.ennemiPrecedent);
        } else {
        	switch(this.ennemiPrecedent) {
        	case 0:
        		if (!heroJoueur2.get(1).isDead()) {
        			archerTargetIndex = 1;
        			this.ennemiPrecedent = 1;
        		} else {
        			archerTargetIndex = BattleMain.iaGetCiblePrioritaire(index, plateauAnalyse, nbTour, null);
        		}
        		break;
        	case 1:
            		if (!heroJoueur2.get(2).isDead()) {
            			archerTargetIndex = 2;
            			this.ennemiPrecedent = 2;
            		} else {
            			archerTargetIndex = BattleMain.iaGetCiblePrioritaire(index, plateauAnalyse, nbTour, null);
            		}
            		break;
            case 2:
              		if (!heroJoueur2.get(0).isDead()) {
             			archerTargetIndex = 0;
             			this.ennemiPrecedent = 0;
                		} else {
                			archerTargetIndex = BattleMain.iaGetCiblePrioritaire(index, plateauAnalyse, nbTour, null);
                		}
                		break;
        	}
        }
        
        if (!archer.isDead()) {
            if (archer.getCurrentMana() >= 2) {
                action = BattleMain.genereAction(plateauAnalyse, index, "FIREBOLT", archerTargetIndex, 1);
            } else {
                action = BattleMain.genereAction(plateauAnalyse, index, "REST", index, 0);
            }
        }
        
        return action;
    }
    
    public static String genereAction(ArrayList < ArrayList < Hero >> plateauAnalyse, int hero1, String attaque, int hero2, int versJoueur) {
        String action = "";
        ArrayList < Hero > heroJoueur1 = plateauAnalyse.get(0);
        ArrayList < Hero > heroJoueur2 = plateauAnalyse.get(1);
        int mort = 0;

        for (int i = 0; i < heroJoueur1.size(); i++) {
            if (heroJoueur1.get(i).getFighterClass().equals(heroJoueur1.get(hero1).getFighterClass())) {
                mort = i + 1;
                action = "A" + mort + "," + attaque + ",";

                switch (versJoueur) {
                    case 1:
                        for (int j = 0; j < heroJoueur2.size(); j++) {
                            if (heroJoueur2.get(j).getFighterClass().equals(heroJoueur2.get(hero2).getFighterClass())) {
                                mort = j + 1;
                                action += "E" + mort;
                            }
                        }
                        break;
                    case 0:
                        for (int j = 0; j < heroJoueur1.size(); j++) {
                            if (heroJoueur1.get(j).getFighterClass().equals(heroJoueur1.get(hero2).getFighterClass())) {
                                mort = j + 1;
                                action += "A" + mort;
                            }
                        }
                        break;
                    default:
                        action = "rien";
                        break;
                }
            }
		}
		
        return action;
    }

    public static ArrayList < ArrayList < Hero >> analysePlateau(String plateau) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(plateau);
        ArrayList < Hero > heroj1 = new ArrayList < Hero > ();
        ArrayList < Hero > heroj2 = new ArrayList < Hero > ();
        JsonObject joueur1 = jo.get("playerBoards").getAsJsonArray().get(0).getAsJsonObject();
		JsonObject joueur2 = jo.get("playerBoards").getAsJsonArray().get(1).getAsJsonObject();
		
        if (joueur1.get("fighters").isJsonArray()) {
            int nbHero = joueur1.get("fighters").getAsJsonArray().size();
            for (int i = 0; i < nbHero; i++) {
                heroj1.add(BattleMain.JsonObjectToHero(joueur1.get("fighters").getAsJsonArray().get(i).getAsJsonObject()));
                heroj2.add(BattleMain.JsonObjectToHero(joueur2.get("fighters").getAsJsonArray().get(i).getAsJsonObject()));
            }
        }
        ArrayList < ArrayList < Hero >> analyse = new ArrayList < ArrayList < Hero >> ();
        
        if (joueur1.get("playerName").getAsString().equals("Quadrifusion")) {
        	analyse.add(heroj1);
            analyse.add(heroj2);
        } else {
        	analyse.add(heroj2);
            analyse.add(heroj1);
        }

        return analyse;
    }

    public static Hero JsonObjectToHero(JsonObject jo) {
		Hero hero = new Hero();
		
        hero.setFighterID(jo.get("fighterID").getAsString());
        hero.setFighterClass(jo.get("fighterClass").getAsString());
        hero.setCurrentLife(jo.get("currentLife").getAsInt());
        hero.setDead(jo.get("isDead").getAsBoolean());
        hero.setCurrentMana(jo.get("currentMana").getAsInt());
        hero.setMaxAvailableLife(jo.get("maxAvailableLife").getAsInt());
        hero.setMaxAvailableMana(jo.get("maxAvailableMana").getAsInt());
        hero.setOrderNumberInTeam(jo.get("orderNumberInTeam").getAsInt());
        if (!jo.get("states").isJsonNull()) {
            if (jo.get("states").getAsJsonArray() != null) {
                hero.setStates(jo.get("states").getAsJsonArray());
            }
		}
		
        return hero;
    }

    public static void main(String[] args) throws Exception {
        Integer nbTourMax = 100;
        Integer nbTour = 1;
		BattleMain battleMain = new BattleMain(args);
		
        if (battleMain.getTeamName() == null || battleMain.getTeamPwd() == null) {
            throw new Exception("Ajoutez le parametre '-config'");
        }

        Gson gson = new GsonBuilder()
            .setLenient()
            .create();

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(battleMain.getBaseUrl())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

        CodeandplayService service = retrofit.create(CodeandplayService.class);

        try {
            if (!service.ping().execute().body().equals("pong")) {
                throw new Exception("Le serveur est injoignable.");
            } else {
                System.out.println("Connectee !");
            }

            String idEquipe = (String) service.getIdEquipe(battleMain.getTeamName(), battleMain.getTeamPwd()).execute().body();
            System.out.println("L'ID de l'equipe est : " + idEquipe);

            String modeDeJeu = battleMain.getInputModeDeJeu();
            String idPartie = "NA";
            if (modeDeJeu.equals("VERSUS")) {
                System.out.println("En attente d'une partie contre un adversaire...");
                while (idPartie.equals("NA")) {
                    idPartie = (String) service.nouvellePartieContreJoueurs(idEquipe).execute().body();
                }
                System.out.println("Adversaire trouv� !");
            } else if (modeDeJeu.equals("BOTS")) {
                String niveauDuBot = battleMain.getInputNiveauBot();
                while (idPartie.equals("NA")) {
                    idPartie = (String) service.nouvellePartieContreBots(niveauDuBot, idEquipe).execute().body();
                }
            } else {
                throw new Exception("Mode de jeu inconnu.");
            }

            System.out.println("L'ID de la partie est : " + idPartie);

            String statut = service.getStatusPartie(idPartie, idEquipe).execute().body();
            String action = "rien";
            String resultatAction = "NOTYET";
            String plateau = null;

            while (!statut.equals("VICTORY") && !statut.equals("DEFEAT") && !statut.equals("CANCELLED") && !statut.equals("DRAW") && !statut.equals("GAMEOVER")) {
                statut = service.getStatusPartie(idPartie, idEquipe).execute().body();
                plateau = service.getPlateauPartie(idPartie, "json").execute().body().string();
                System.out.println(statut);
                action = battleMain.getAction(statut, BattleMain.analysePlateau(plateau), nbTour);
                resultatAction = "NOTYET";
                if (!action.equals("rien") && !action.equals("$") && !action.equals("")) {
                    while (resultatAction.equals("FORBIDDEN") || resultatAction.equals("NOTYET")) {
                        resultatAction = service.jouerUnTour(idPartie, idEquipe, action).execute().body();
                        if (resultatAction != null) {
                        	if (resultatAction.equals("OK")) {
                                nbTour++;
                            }
                        }
                        System.out.println(resultatAction);
                        System.out.println(plateau);
                        if (nbTour > 3) {
                            TimeUnit.SECONDS.sleep(0);
                        }
                    }
                }
                if (nbTourMax == nbTour) {
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}